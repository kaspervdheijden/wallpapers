# Wallpapers

Set of used wallpapers for the GNOME desktop.

Under `scripts/`, there is a wallpaper rotator daemon. This can be run as a system service
or in the desktop environment.

## To run as a service:
Assumed to be in the repository root director (do **not** run as root):
```sh
./scripts/setup.sh
```
