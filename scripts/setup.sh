#!/bin/env sh

if [ "$(id -u)" -eq 0 ]; then
    echo 'do not run as root' >&2
    return 3
fi

root_dir="$(git rev-parse --show-toplevel 2>/dev/null)"
if [ ! -d "${root_dir}" ]; then
    echo "script must be run from within repository: not a valid directory: '${root_dir}'" >&2
    exit 4
fi

if ! mkdir -p "${HOME}/.config/systemd/user"; then
    echo "could not create '${HOME}/.config/systemd/user'" >&2
    exit 5
fi

while true; do
    printf 'timeout (in seconds): '
    read -r interval

    if ! printf '%s\n' "${interval}" | grep -qE '^[0-9]+$'; then
        echo "not a number: ${interval}" >&2
    elif [ "${interval}" -lt 30 ]; then
        echo 'number must be at least 30' >&2
    else
        break
    fi
done

random_flag='-r '
printf 'display images randomly? [Y/n]: '
read -r answer && printf '%s\n' "${answer}" | grep -Eiq '^n' && random_flag=''

cat > "${HOME}/.config/systemd/user/wallpaper-rotator.service" <<END_OF_FILE
[Unit]
Description=Wallpaper rotator

[Service]
ExecStart=${root_dir}/scripts/wallpaper-rotator-daemon ${random_flag}-t${interval} -d${root_dir}

[Install]
WantedBy=default.target
END_OF_FILE

systemctl --user daemon-reload 2>/dev/null
systemctl --user enable wallpaper-rotator
systemctl --user restart wallpaper-rotator
systemctl --user status wallpaper-rotator
